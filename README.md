# pykkn

PYKKN is a library that supports researchers to store their data in a HDF5 file using Python. The library can handle different types of inputs like array, matrix, image and video.

## Documentation
The documentation can be found [here](https://fst-tuda.pages.rwth-aachen.de/public/pykkn/).

## Repository
https://git.rwth-aachen.de/fst-tuda/public/pykkn


## Acknowledgements

This software was created at the [Chair of Fluid Systems (FST)](https://www.fst.tu-darmstadt.de/), [TU Darmstadt](https://www.tu-darmstadt.de) within the project [NFDI4Ing](https://www.nfdi4ing.de).

The authors would like to thank the Federal Government and the Heads of Government of the Länder, as well as the Joint Science Conference (GWK), for their funding and support within the framework of the NFDI4Ing consortium. Funded by the German Research Foundation (DFG) - project number [442146713](https://gepris.dfg.de/gepris/projekt/442146713?context=projekt&task=showDetail&id=442146713&).

