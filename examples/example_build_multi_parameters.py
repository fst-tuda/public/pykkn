from pykkn.build_multi_parameters import build_multi_parameters

dic = {
    "list_of_parameters": [
        {"name": "fan/diameter", "value": 1235, "units": 'm'},
        {"name": "name_of_parameter_2", "value": 456},
        {"name": "name_of_parameter_3", "value": 789},
    ]
}

list_parameters = build_multi_parameters(dic)
print(list_parameters)

for parameter in list_parameters:
    parameter.show()
