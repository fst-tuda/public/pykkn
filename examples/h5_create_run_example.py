import numpy as np
from pykkn.dataset import Dataset
from pykkn.instrument import Instrument
from pykkn.model import Model
from pykkn.parameter import Parameter
from pykkn.pipeline import Pipeline
from pykkn.run import Run

if __name__ == "__main__":

    dataset1 = Dataset("msmt01")
    dataset1.data = np.random.rand(1, 10)
    dataset1.attrs["samplerate"] = 1000
    dataset1.attrs["timestamp"] = "2017-05-14 18:44:11"

    dataset2 = Dataset("msmt02")
    dataset2.data = np.ones((1, 10))
    dataset2.attrs["samplerate"] = 1000
    dataset2.attrs["timestamp"] = "2017-05-14 19:44:11"

    parameter1 = Parameter("gain")
    parameter1.attrs["value"] = 1
    parameter1.attrs["units"] = "-"
    parameter1.attrs["variable"] = "-"
    parameter1.attrs["origin"] = "this"

    parameter2 = Parameter("offset")
    parameter2.attrs["value"] = 0
    parameter2.attrs["units"] = "volts"
    parameter2.attrs["variable"] = "voltage"
    parameter2.attrs["origin"] = "this"

    parameter3 = Parameter("gain")
    parameter3.attrs["value"] = 10
    parameter3.attrs["units"] = "pascals/volts"
    parameter3.attrs["variable"] = "pressure/voltage"
    parameter3.attrs["origin"] = "this"

    parameter4 = Parameter("offset")
    parameter4.attrs["value"] = 5
    parameter4.attrs["units"] = "pascals"
    parameter4.attrs["variable"] = "pressure"
    parameter4.attrs["origin"] = "this"

    model1 = Model("feedthrough")
    model1.add([parameter1, parameter2])

    model2 = Model("linear")
    model2.add([parameter3, parameter4])

    instrument1 = Instrument("transmitter")
    instrument1.add([model1])

    instrument2 = Instrument("amplifier")
    instrument2.add([model2])

    pipeline1 = Pipeline("measured/capa1/raw")
    pipeline1.add([dataset1, dataset2])
    pipeline1.attrs["variable"] = "voltage"
    pipeline1.attrs["units"] = "volts"
    pipeline1.attrs["origin"] = "this"

    pipeline2 = Pipeline("measured/capa1/scaled")
    pipeline2.add([instrument1, instrument2])
    pipeline2.attrs["variable"] = "distance"
    pipeline2.attrs["units"] = "micrometers"
    pipeline2.attrs["origin"] = pipeline1.name

    pipeline3 = Pipeline("measured/capa2/raw")
    pipeline3.add([dataset1, dataset2])
    pipeline3.attrs["variable"] = "voltage"
    pipeline3.attrs["units"] = "volts"
    pipeline3.attrs["origin"] = "this"

    pipeline4 = Pipeline("measured/capa2/scaled")
    pipeline4.add([instrument1, instrument2])
    pipeline4.attrs["variable"] = "distance"
    pipeline4.attrs["units"] = "micrometers"
    pipeline4.attrs["origin"] = pipeline3.name

    msmtrun = Run("run2")
    msmtrun.attrs["author"] = "derGeraet"
    msmtrun.attrs["pmanager"] = "tcorneli"
    msmtrun.attrs["targettmp"] = np.double(70)
    msmtrun.attrs["targetrps"] = np.double(2)
    msmtrun.attrs["oil"] = "PA06"

    msmtrun.add([pipeline1, pipeline2, pipeline3, pipeline4])

    param5 = Parameter("fan/diameter")
    param5.attrs["value"] = 1235
    param5.attrs["units"] = "m"
    param5.attrs["variable"] = "-"
    param5.attrs["origin"] = "this"

    param6 = Parameter("opration/rpm")
    param6.attrs["value"] = 3000
    param6.attrs["units"] = "1/minutes"
    param6.attrs["variable"] = "frequency"
    param6.attrs["origin"] = "this"

    msmtrun.add([param5, param6])

    msmtrun.set_storage_path("tests/test_results/h5_create_run_example.h5")

    msmtrun.store(format="json")
