import numpy as np

from pykkn.dataset import Dataset
from pykkn.parameter import Parameter
from pykkn.model import Model
from pykkn.instrument import Instrument
from pykkn.pipeline import Pipeline
from pykkn.run import Run

from pykkn.parse import pykkn_parse


dataset1 = Dataset("msmt01")
dataset1.data = np.random.rand(1, 10)
dataset1.attrs["samplerate"] = 1000
dataset1.attrs["timestamp"] = "2017-05-14 18:44:11 "

parameter1 = Parameter("gain")
parameter1.attrs["value"] = 1
parameter1.attrs["units"] = "-"
parameter1.attrs["variable"] = "-"
parameter1.attrs["origin"] = "this"

model1 = Model("feedthrough")
model1.add([parameter1])

instrument1 = Instrument("transmitter")
instrument1.add([model1])

pipeline1 = Pipeline("measured/capa1/raw")
pipeline1.add([dataset1, instrument1])
pipeline1.attrs["variable"] = "voltage"
pipeline1.attrs["units"] = "volts"
pipeline1.attrs["origin"] = "this"

msmtrun = Run("run1")
msmtrun.attrs["author"] = "derGeraet"
msmtrun.attrs["pmanager"] = "tcorneli"
msmtrun.attrs["targettmp"] = np.double(70)
msmtrun.attrs["targetrps"] = np.double(2)
msmtrun.attrs["oil"] = "PA06"

msmtrun.add([pipeline1])

msmtrun.add([parameter1])

msmtrun.set_storage_path("example_for_docs.h5")

msmtrun.store()


result_msmtrun = pykkn_parse("example_for_docs.h5")
result_msmtrun.show()
