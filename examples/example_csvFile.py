import pandas as pd
from pykkn.dataset import Dataset

if __name__ == "__main__":

    df = pd.read_csv("exampleTable.csv", sep=';')

    dataset1 = Dataset("from_csv_table")
    dataset1.data = df.values

    dataset1.set_storage_path("from_csv_table.h5")

    dataset1.store()
