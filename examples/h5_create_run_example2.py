from pykkn.instrument import Instrument
from pykkn.model import Model
from pykkn.parameter import Parameter
from pykkn.pipeline import Pipeline
from pykkn.run import Run

filename = "tests/test_results/h5_create_run_example2.h5"
Runname = "input"
pump_type = "input"
pump_manufacturer = "input"
testrig_name = "input"
msmt_type = "input"
project = "input"
oil_type = "input"
oil_ID = "input"
project_manager = "input"
author = "input"
description = "input"
comment = "input"
timestamp = "input"
opt_in = "input"

flowmeter = "RS1"
project = "AIF - Experimentelle ValidieRung eines '\
    'typenunabhaengigen Wirkungsgradmodells von Verdraengerpumpen"


# Basic Info

# Run object with extra information
msmtRun = Run(Runname)
msmtRun.set_storage_path(filename)
msmtRun.attrs["author"] = author
msmtRun.attrs["pmanager"] = project_manager
msmtRun.attrs["msmt_type"] = msmt_type
msmtRun.attrs["testrig_name"] = testrig_name
msmtRun.attrs["project"] = project
msmtRun.attrs["oil_type"] = oil_type
msmtRun.attrs["oil_ID"] = oil_ID
msmtRun.attrs["timestamp_created"] = timestamp

msmtRun.attrs["pump_type"] = pump_type
msmtRun.attrs["pump_manufacturer"] = pump_manufacturer
msmtRun.attrs["description"] = description
msmtRun.attrs["comment"] = comment


# Channel_01

# pressure_1 raw
# create Instruments
converter_01_raw = Instrument("CDAQ9189")  # #KKNREPLACEMENT
converter_01_raw.attrs["comment"] = ""
converter_01_raw.attrs["description"] = ""

converter_01_raw2 = Instrument("Module1-NI9215")  # #KKNREPLACEMENT
converter_01_raw2.attrs["comment"] = ""
converter_01_raw2.attrs["description"] = ""


# channel object for VOLTAGE values of pressure_1 mesurement
channel_01_raw = Pipeline("measured/pressure_1/raw")  # #KKNREPLACEMENT
channel_01_raw.add([converter_01_raw, converter_01_raw2])
channel_01_raw.attrs["variable"] = "voltage"
channel_01_raw.attrs["units"] = "volts"
channel_01_raw.attrs["origin"] = "this"


# pressure_1 scaled
# set Parameters for computing model
param_pressure1 = Parameter("gain")
param_pressure1.attrs["value"] = 0.25
param_pressure1.attrs["units"] = "bar/volts"
param_pressure1.attrs["variable"] = "pressure/voltage"
param_pressure1.attrs["origin"] = "datasheet"

param_pressure2 = Parameter("offset")
param_pressure2.attrs["value"] = 0
param_pressure2.attrs["units"] = "bar"
param_pressure2.attrs["variable"] = "pressure"
param_pressure2.attrs["origin"] = "datasheet"

# create computing model
model = Model("linear")  # #KKNREPLACEMENT
model.add([param_pressure1, param_pressure2])

# Instrument object for measurement device
converter_01 = Instrument("pressure_sensor")  # #KKNREPLACEMENT
converter_01.add([model])
converter_01.attrs["device_type"] = "PBMN 25B18AA24406211000"
converter_01.attrs["manufacturer"] = "Baumer GmbH"
converter_01.attrs["serial_number"] = "2.17.101929876.1"
converter_01.attrs["timestamp_calibrated"] = "2017-09-04"
converter_01.attrs["input_range_min"] = 0
converter_01.attrs["input_range_max"] = 2.5
converter_01.attrs["input_units"] = "bar absolut"
converter_01.attrs["output_range_min"] = 0
converter_01.attrs["output_range_max"] = 10
converter_01.attrs["output_units"] = "volts"
converter_01.attrs["description"] = ""
converter_01.attrs["comment"] = ""
converter_01.attrs["accuracy"] = 0.15
converter_01.attrs["accuracy_type"] = "#fullscale"

# Pipeline object for pressure_1 values of pressure measurement
channel_01_scaled = Pipeline("measured/pressure_1/scaled")  # #KKNREPLACEMENT
channel_01_scaled.add([converter_01])
channel_01_scaled.attrs["variable"] = "pressure"
channel_01_scaled.attrs["units"] = "bar"
channel_01_scaled.attrs["origin"] = "measured/pressure_1/raw"


# Channel_02

# pressure_pump_out raw
# create Instruments
converter_02_raw = Instrument("CDAQ9189")  # #KKNREPLACEMENT
converter_02_raw.attrs["comment"] = ""
converter_02_raw.attrs["description"] = ""

converter_02_raw2 = Instrument("Module1-NI9215")  # #KKNREPLACEMENT
converter_02_raw2.attrs["comment"] = ""
converter_02_raw2.attrs["description"] = ""

# Pipeline object for VOLTAGE values of Pressure_valve_in mesurement
channel_02_raw = Pipeline("measured/pressure_2/raw")  # #KKNREPLACEMENT
channel_02_raw.add([converter_02_raw, converter_02_raw2])
channel_02_raw.attrs["variable"] = "voltage"
channel_02_raw.attrs["units"] = "volts"
channel_02_raw.attrs["origin"] = "this"


# Pressure_valve_in scaled
# set necessary Parameters for computing model
param_pressure1 = Parameter("gain")
param_pressure1.attrs["value"] = 2.5
param_pressure1.attrs["units"] = "bar/volts"
param_pressure1.attrs["variable"] = "pressure/voltage"
param_pressure1.attrs["origin"] = "datasheet"

param_pressure2 = Parameter("offset")
param_pressure2.attrs["value"] = 0
param_pressure2.attrs["units"] = "bar"
param_pressure2.attrs["variable"] = "pressure"
param_pressure2.attrs["origin"] = "datasheet"

# computing model of Pressure_valve_in values
model = Model("linear")  # #KKNREPLACEMENT
model.add([param_pressure1, param_pressure2])

# Instrument object for measurement device
converter_02 = Instrument("pressure_sensor")  # #KKNREPLACEMENT
converter_02.add([model])
converter_02.attrs["device_type"] = "PBMN 25B26AA24406211000"
converter_02.attrs["manufacturer"] = "Baumer GmbH"
converter_02.attrs["serial_number"] = "2.17.101929865.2"
converter_02.attrs["timestamp_calibrated"] = "2017-09-04"
converter_02.attrs["input_range_min"] = 0
converter_02.attrs["input_range_max"] = 25
converter_02.attrs["input_units"] = "bar absolut"
converter_02.attrs["output_range_min"] = 0
converter_02.attrs["output_range_max"] = 10
converter_02.attrs["output_units"] = "volts"
converter_02.attrs["description"] = ""
converter_02.attrs["comment"] = ""
converter_02.attrs["accuracy"] = 0.15
converter_02.attrs["accuracy_type"] = "#fullscale"

# Pipeline object for Pressure_valve_in values of pressure measurement
channel_02_scaled = Pipeline("measured/pressure_2/scaled")  # #KKNREPLACEMENT
channel_02_scaled.add([converter_02])
channel_02_scaled.attrs["variable"] = "pressure"
channel_02_scaled.attrs["units"] = "bar"
channel_02_scaled.attrs["origin"] = "measured/pressure_2/raw"


# Channel_03

# pressure_3 raw
# create Instruments
converter_03_raw = Instrument("CDAQ9189")  # #KKNREPLACEMENT
converter_03_raw.attrs["comment"] = ""
converter_03_raw.attrs["description"] = ""

converter_03_raw2 = Instrument("Module1-NI9215")  # #KKNREPLACEMENT
converter_03_raw2.attrs["comment"] = ""
converter_03_raw2.attrs["description"] = ""

# Pipeline object for VOLTAGE values of pressure_3 mesurement
channel_03_raw = Pipeline("measured/pressure_3/raw")  # #KKNREPLACEMENT
channel_03_raw.add([converter_03_raw])
channel_03_raw.add([converter_03_raw2])
channel_03_raw.attrs["variable"] = "voltage"
channel_03_raw.attrs["units"] = "volts"
channel_03_raw.attrs["origin"] = "this"


# pressure_3 scaled
# set necessary Parameters for computing model
param_pressure1 = Parameter("gain")
param_pressure1.attrs["value"] = 1.5
param_pressure1.attrs["units"] = "bar/volts"
param_pressure1.attrs["variable"] = "pressure/voltage"
param_pressure1.attrs["origin"] = "datasheet"

param_pressure2 = Parameter("offset")
param_pressure2.attrs["value"] = 0
param_pressure2.attrs["units"] = "bar"
param_pressure2.attrs["variable"] = "pressure"
param_pressure2.attrs["origin"] = "datasheet"

# computing model of pressure_3 values
model = Model("linear")  # #KKNREPLACEMENT
model.add([param_pressure1, param_pressure2])

# Instrument object for measurement device
converter_03 = Instrument("pressure_sensor")  # #KKNREPLACEMENT
converter_03.add([model])
converter_03.attrs["device_type"] = "PAA 23/78465 15"
converter_03.attrs["manufacturer"] = "KELLER AG"
converter_03.attrs["serial_number"] = "26/8465 15"
converter_03.attrs["timestamp_calibrated"] = ""
converter_03.attrs["input_range_min"] = 0
converter_03.attrs["input_range_max"] = 15
converter_03.attrs["input_units"] = "bar absolut"
converter_03.attrs["output_range_min"] = 0
converter_03.attrs["output_range_max"] = 10
converter_03.attrs["output_units"] = "volts"
converter_03.attrs["description"] = ""
converter_03.attrs["comment"] = ""
converter_03.attrs["accuracy"] = 0.2
converter_03.attrs["accuracy_type"] = "#fullscale"

# Pipeline object for pressure_3 values of pressure measurement
channel_03_scaled = Pipeline("measured/pressure_3/scaled")  # #KKNREPLACEMENT
channel_03_scaled.add([converter_03])
channel_03_scaled.attrs["variable"] = "pressure"
channel_03_scaled.attrs["units"] = "bar"
channel_03_scaled.attrs["origin"] = "measured/pressure_3/raw"


# Channel_04
# temperature_1 raw
# create Instruments
converter_04_raw = Instrument("CDAQ9189")  # #KKNREPLACEMENT
converter_04_raw.attrs["comment"] = ""
converter_04_raw.attrs["description"] = ""

converter_04_raw2 = Instrument("Module8-NI9217")  # #KKNREPLACEMENT
converter_04_raw2.attrs["comment"] = ""
converter_04_raw2.attrs["description"] = ""

# Pipeline object for Temp values of Temp1 mesurement
channel_04_raw = Pipeline("measured/temperature_1/raw")  # #KKNREPLACEMENT
channel_04_raw.add([converter_04_raw])
channel_04_raw.add([converter_04_raw2])
channel_04_raw.attrs["variable"] = "temperature"
channel_04_raw.attrs["units"] = "deg_C"
channel_04_raw.attrs["origin"] = "this"

# temperature_1 scaled


param_temp1 = Parameter("gain")
param_temp1.attrs["value"] = -258.68
param_temp1.attrs["units"] = "deg_C/ohm"
param_temp1.attrs["variable"] = "temperature/resistance"
param_temp1.attrs["origin"] = "calibration with TTD calibration thermometer"

param_temp2 = Parameter("offset")
param_temp2.attrs["value"] = 2.584
param_temp2.attrs["units"] = "deg_C"
param_temp2.attrs["variable"] = "temperature"
param_temp2.attrs["origin"] = "calibration with TTD calibration thermometer"

# computing model of temperature_1 values
model = Model("linear")  # #KKNREPLACEMENT
model.add([param_temp1, param_temp2])

# Instrument object for measurement device
converter_04 = Instrument("Pt100")  # #KKNREPLACEMENT
converter_04.add([model])
converter_04.attrs["device_type"] = "WIKA TR33-T-P4"
converter_04.attrs["manufacturer"] = "WIKA Alexander Wiegand SE & Co. KG"
converter_04.attrs["serial_number"] = "1A00LHR3YJ6"
converter_04.attrs["timestamp_calibrated"] = "2017-09-22"
converter_04.attrs["input_range_min"] = 0
converter_04.attrs["input_range_max"] = 100
converter_04.attrs["input_units"] = "deg_C"
converter_04.attrs["output_range_min"] = 0
converter_04.attrs["output_range_max"] = 140
converter_04.attrs["output_units"] = "Ohm"
converter_04.attrs["description"] = ""
converter_04.attrs["comment"] = ""
converter_04.attrs["accuracy"] = 0.15
converter_04.attrs["accuracy_type"] = "absolute"

# channel object for temperature_1 values of temperature measurement
channel_04_scaled = Pipeline("measured/temperature_1/scaled")
channel_04_scaled.add([converter_04])
channel_04_scaled.attrs["variable"] = "temperature"
channel_04_scaled.attrs["units"] = "deg_C"
channel_04_scaled.attrs["origin"] = "measured/temperature_1/raw"


# Channel_05

# temperature_2 raw (+Temp_Valve_in)

converter_05_raw = Instrument("CDAQ9189")  # #KKNREPLACEMENT
converter_05_raw.attrs["comment"] = ""
converter_05_raw.attrs["description"] = ""

converter_05_raw2 = Instrument("Module8-NI9217")  # #KKNREPLACEMENT
converter_05_raw2.attrs["comment"] = ""
converter_05_raw2.attrs["description"] = ""

# Pipeline object for Temp values of temperature_2 mesurement
channel_05_raw = Pipeline("measured/temperature_2/raw")  # #KKNREPLACEMENT
channel_05_raw.add([converter_05_raw])
channel_05_raw.add([converter_05_raw2])
channel_05_raw.attrs["variable"] = "temperature"
channel_05_raw.attrs["units"] = "deg_C"
channel_05_raw.attrs["origin"] = "this"


#  temperature_2 scaled (+Temp_Valve_in)
# set necessary Parameters for computing model
param_temp1 = Parameter("gain")
param_temp1.attrs["value"] = -258.98
param_temp1.attrs["units"] = "deg_C/ohm"
param_temp1.attrs["variable"] = "temperature/resistance"
param_temp1.attrs["origin"] = "calibration with TTD calibration thermometer"

param_temp2 = Parameter("offset")
param_temp2.attrs["value"] = 2.585
param_temp2.attrs["units"] = "deg_C"
param_temp2.attrs["variable"] = "temperature"
param_temp2.attrs["origin"] = "calibration with TTD calibration thermometer"

# computing model of Temp2 values
model = Model("linear")  # #KKNREPLACEMENT
model.add([param_temp1, param_temp2])

# Instrument object for measurement device
converter_05 = Instrument("Pt100")  # #KKNREPLACEMENT
converter_05.add([model])
converter_05.attrs["device_type"] = "TR33-T-P4"
converter_05.attrs["manufacturer"] = "WIKA Alexander Wiegand SE & Co. KG"
converter_05.attrs["serial_number"] = "1A00LHR1KIQ"
converter_05.attrs["timestamp_calibrated"] = "2017-09-22"
converter_05.attrs["input_range_min"] = 0
converter_05.attrs["input_range_max"] = 100
converter_05.attrs["input_units"] = "deg_C"
converter_05.attrs["output_range_min"] = 0
converter_05.attrs["output_range_max"] = 140
converter_05.attrs["output_units"] = "Ohm"
converter_05.attrs["description"] = ""
converter_05.attrs["comment"] = ""
converter_05.attrs["accuracy"] = 0.15
converter_05.attrs["accuracy_type"] = "absolute"

# Pipeline object for temperature_2 values of temperature measurement
channel_05_scaled = Pipeline("measured/temperature_2/scaled")
channel_05_scaled.add([converter_05])
channel_05_scaled.attrs["variable"] = "temperature"
channel_05_scaled.attrs["units"] = "deg_C"
channel_05_scaled.attrs["origin"] = "measured/temperature_2/raw"


# Channel_06
# Temp_Tank raw
# create Instruments
converter_06_raw = Instrument("CDAQ9189")  # #KKNREPLACEMENT
converter_06_raw.attrs["comment"] = ""
converter_06_raw.attrs["description"] = ""

converter_06_raw2 = Instrument("Module8-NI9217")  # #KKNREPLACEMENT
converter_06_raw2.attrs["comment"] = ""
converter_06_raw2.attrs["description"] = ""

# Pipeline object for TEMP values of Temp_Tank mesurement
channel_06_raw = Pipeline("measured/temperature_tank/raw")  # #KKNREPLACEMENT
channel_06_raw.add([converter_06_raw])
channel_06_raw.add([converter_06_raw2])
channel_06_raw.attrs["variable"] = "temperature"
channel_06_raw.attrs["units"] = "deg_C"
channel_06_raw.attrs["origin"] = "this"

# Temp_Tank scaled
# set necessary Parameters for computing model
param_temp1 = Parameter("R_0")
param_temp1.attrs["value"] = 100
param_temp1.attrs["units"] = "Ohm"
param_temp1.attrs["variable"] = "resistance"
param_temp1.attrs["origin"] = "datasheet"

param_temp2 = Parameter("A")
param_temp2.attrs["value"] = 3.9083e-3
param_temp2.attrs["units"] = "deg_C^-1"
param_temp2.attrs["variable"] = "temperature^-1"
param_temp2.attrs["origin"] = "datasheet"

param_temp3 = Parameter("B")
param_temp3.attrs["value"] = -5.7750e-7
param_temp3.attrs["units"] = "deg_C^-2"
param_temp3.attrs["variable"] = "temperature^-2"
param_temp3.attrs["origin"] = "datasheet"

# computing model of Temp_Tank values
model = Model("Calendar-Van-Dusen equation")  # #KKNREPLACEMENT
model.add([param_temp1, param_temp2, param_temp3])

# Instrument object for measurement device
converter_06 = Instrument("Pt100")  # #KKNREPLACEMENT
converter_06.add([model])
converter_06.attrs["device_type"] = "ESW7G1/2-AA-400"
converter_06.attrs["manufacturer"] = "Temperatur Messelemente Hettstedt GmbH"
converter_06.attrs["serial_number"] = "Nr.4"
converter_06.attrs["timestamp_calibrated"] = "2017-08-10"
converter_06.attrs["input_range_min"] = 0
converter_06.attrs["input_range_max"] = 100
converter_06.attrs["input_units"] = "deg_C"
converter_06.attrs["output_range_min"] = 0
converter_06.attrs["output_range_max"] = 140
converter_06.attrs["output_units"] = "Ohm"
converter_06.attrs["description"] = ""
converter_06.attrs["comment"] = ""
converter_06.attrs["accuracy"] = 0.7
converter_06.attrs["accuracy_type"] = "absolute"

# channel object for Temp_Tank values of temperature measurement
channel_06_scaled = Pipeline("measured/temperature_tank/scaled")
channel_06_scaled.add([converter_06])
channel_06_scaled.attrs["variable"] = "temperature"
channel_06_scaled.attrs["units"] = "deg_C"
channel_06_scaled.attrs["origin"] = "measured/temperature_tank/raw"


# Channel_07
# Torquemeter raw
# create Instruments
converter_07_raw = Instrument("CDAQ9189")  # #KKNREPLACEMENT
converter_07_raw.attrs["comment"] = ""
converter_07_raw.attrs["description"] = ""

converter_07_raw2 = Instrument("Module2-NI9215")  # #KKNREPLACEMENT
converter_07_raw2.attrs["comment"] = ""
converter_07_raw2.attrs["description"] = ""

# Pipeline object for VOLTAGE values of torque measurement
channel_07_raw = Pipeline("measured/torque/raw")  # #KKNREPLACEMENT
channel_07_raw.add([converter_07_raw])
channel_07_raw.add([converter_07_raw2])
channel_07_raw.attrs["variable"] = "voltage"
channel_07_raw.attrs["units"] = "volts"
channel_07_raw.attrs["origin"] = "this"

# Torquemeter
# set necessary Parameters for computing model
param_torque1 = Parameter("gain")
param_torque1.attrs["value"] = 5
param_torque1.attrs["units"] = "Nm/volts"
param_torque1.attrs["variable"] = "torque/voltage"
param_torque1.attrs["origin"] = "calibration_certificate"

param_torque2 = Parameter("offset")
param_torque2.attrs["value"] = 0
param_torque2.attrs["units"] = "Nm"
param_torque2.attrs["variable"] = "torque"
param_torque2.attrs["origin"] = "calibration_certificate"

# computing model of torque values
model = Model("linear")  # #KKNREPLACEMENT
model.add([param_torque1, param_torque2])

# Instrument object for measurement device
converter_07 = Instrument("torque_meter")  # #KKNREPLACEMENT
converter_07.add([model])
converter_07.attrs["device_type"] = "4503A50L00B1C00"
converter_07.attrs["manufacturer"] = "Kistler Instrumente GmbH"
converter_07.attrs["serial_number"] = "098927"
converter_07.attrs["timestamp_calibrated"] = "2007-04-27"
converter_07.attrs["input_range_min"] = -50
converter_07.attrs["input_range_max"] = 50
converter_07.attrs["input_units"] = "Nm"
converter_07.attrs["output_range_min"] = -10
converter_07.attrs["output_range_max"] = 10
converter_07.attrs["output_units"] = "volts"
converter_07.attrs["description"] = ""
converter_07.attrs["comment"] = ""
converter_07.attrs["accuracy"] = 0.15
converter_07.attrs["accuracy_type"] = "#fullscale"

channel_07_scaled = Pipeline("measured/torque/scaled")  # #KKNREPLACEMENT
channel_07_scaled.add([converter_07])
channel_07_scaled.attrs["variable"] = "torque"
channel_07_scaled.attrs["units"] = "Nm"
channel_07_scaled.attrs["origin"] = "measured/torque/raw"


# Channel_08

# rotational_speed raw
# create Instruments
converter_08_raw = Instrument("CDAQ9189")  # #KKNREPLACEMENT
converter_08_raw.attrs["comment"] = ""
converter_08_raw.attrs["description"] = ""

converter_08_raw2 = Instrument("Module5-NI9402")  # #KKNREPLACEMENT
converter_08_raw2.attrs["comment"] = ""
converter_08_raw2.attrs["description"] = ""


# Pipeline object for RPM values of speed measurement
channel_08_raw = Pipeline("measured/rotational_speed/raw")  # #KKNREPLACEMENT
channel_08_raw.add([converter_08_raw])
channel_08_raw.add([converter_08_raw2])
channel_08_raw.attrs["variable"] = "rotational_speed"
channel_08_raw.attrs["units"] = "Hz"
channel_08_raw.attrs["origin"] = "this"

# rotational_speed scaled
# set necessary Parameters for computing model
param_rotspeed1 = Parameter("gain")
param_rotspeed1.attrs["value"] = 1
param_rotspeed1.attrs["units"] = "1"
param_rotspeed1.attrs["variable"] = "1"
param_rotspeed1.attrs["origin"] = "datasheet"

param_rotspeed2 = Parameter("offset")
param_rotspeed2.attrs["value"] = 0
param_rotspeed2.attrs["units"] = "RPM"
param_rotspeed2.attrs["variable"] = "rotational_speed"
param_rotspeed2.attrs["origin"] = "datasheet"

# computing model of RPM values
model = Model("linear")  # #KKNREPLACEMENT
model.add([param_rotspeed1, param_rotspeed2])

# Instrument object for measurement device
converter_08 = Instrument("speed_sensor")  # #KKNREPLACEMENT
converter_08.add([model])
converter_08.attrs["device_type"] = "4503A50L00B1C00"
converter_08.attrs["manufacturer"] = "Kistler Instrumente GmbH"
converter_08.attrs["serial_number"] = "098927"
converter_08.attrs["timestamp_calibrated"] = "2007-04-27"
converter_08.attrs["input_range_min"] = 0
converter_08.attrs["input_range_max"] = 12000
converter_08.attrs["input_units"] = "rpm"
converter_08.attrs["output_range_min"] = 0
converter_08.attrs["output_range_max"] = 720000
converter_08.attrs["output_units"] = "Hz"
converter_08.attrs["description"] = ""
converter_08.attrs["comment"] = ""
converter_08.attrs["accuracy"] = ""
converter_08.attrs["accuracy_type"] = ""

# channel object for RPM values of speed measurement
channel_08_scaled = Pipeline("measured/rotational_speed/scaled")
channel_08_scaled.add([converter_08])
channel_08_scaled.attrs["variable"] = "speed"
channel_08_scaled.attrs["units"] = "RPM"
channel_08_scaled.attrs["origin"] = "measured/rotational_speed/raw"


# Channel_09

# volume_flow raw
# create Instruments
converter_09_raw = Instrument("CDAQ9189")  # #KKNREPLACEMENT
converter_09_raw.attrs["comment"] = ""
converter_09_raw.attrs["description"] = ""

if flowmeter == "RS1":
    converter_09_raw2 = Instrument("Module2-NI9215")
elif flowmeter == "SVC":
    converter_09_raw2 = Instrument("Module5-NI9402")

converter_09_raw2.attrs["comment"] = ""
converter_09_raw2.attrs["description"] = ""

# Pipeline object for VOLTAGE values of flow mesurement
channel_09_raw = Pipeline("measured/volume_flow/raw")  # #KKNREPLACEMENT
channel_09_raw.add([converter_09_raw])
channel_09_raw.add([converter_09_raw2])

if flowmeter == "RS1":
    channel_09_raw.attrs["variable"] = "voltage"
    channel_09_raw.attrs["units"] = "volts"
elif flowmeter == "SVC":
    channel_09_raw.attrs["variable"] = "frequency"
    channel_09_raw.attrs["units"] = "Hz"

channel_09_raw.attrs["origin"] = "this"

# volume_flow scaled
# set necessary Parameters for computing model
param_volflow1 = Parameter("gain")

if flowmeter == "RS1":
    param_volflow1.attrs["value"] = 12.0248
    param_volflow1.attrs["units"] = "l/(min*volts)"
    param_volflow1.attrs["variable"] = "volume flow/voltage"
elif flowmeter == "SVC":
    param_volflow1.attrs["value"] = 0.3078
    param_volflow1.attrs["units"] = "l/(min*Hz)"
    param_volflow1.attrs["variable"] = "volume flow/frequency"

param_volflow1.attrs["origin"] = "calibration_certificate"
param_volflow2 = Parameter("offset")
param_volflow2.attrs["value"] = 0
param_volflow2.attrs["units"] = "l/min"
param_volflow2.attrs["variable"] = "volume flow"
param_volflow2.attrs["origin"] = "calibration_certificate"

# computing model of flow values
model = Model("linear")  # #KKNREPLACEMENT
model.add([param_volflow1, param_volflow2])

# converter object for measurement device
converter_09 = Instrument("flow_meter")  # #KKNREPLACEMENT
converter_09.add([model])

if flowmeter == "RS1":
    converter_09.attrs["device_type"] = "RS 100/025 GR012V-GSM01 ARG 10' \
        '0-E-V-1-0-0-N/2"
    converter_09.attrs["manufacturer"] = "VSE Volumentechnik GmbH"
    converter_09.attrs["serial_number"] = "120/02 084"
    converter_09.attrs["timestamp_calibrated"] = "2007-08-04"
    converter_09.attrs["input_range_min"] = 0
    converter_09.attrs["input_range_max"] = 120
    converter_09.attrs["accuracy"] = 0.3
elif flowmeter == "SVC":
    converter_09.attrs["device_type"] = "SVC 40 A1 G1 F1 S1"
    converter_09.attrs["manufacturer"] = "Pumpmanu2 GmbH"
    converter_09.attrs["serial_number"] = "180311592"
    converter_09.attrs["timestamp_calibrated"] = ""
    converter_09.attrs["input_range_min"] = 0
    converter_09.attrs["input_range_max"] = 600
    converter_09.attrs["accuracy"] = 0.2

converter_09.attrs["input_units"] = "l/min"
converter_09.attrs["description"] = ""
converter_09.attrs["comment"] = ""
converter_09.attrs["accuracy_type"] = "#reading"


# Pipeline object for flow values of flow measurement
channel_09_scaled = Pipeline("measured/volume_flow/scaled")  # #KKNREPLACEMENT
channel_09_scaled.add([converter_09])
channel_09_scaled.attrs["variable"] = "volume_flow"
channel_09_scaled.attrs["units"] = "l/min"
channel_09_scaled.attrs["origin"] = "measured/volume_flow/raw"


# Channel_10

# valve_position raw
# create Instruments
converter_10_raw = Instrument("CDAQ9189")  # #KKNREPLACEMENT
converter_10_raw.attrs["comment"] = ""
converter_10_raw.attrs["description"] = ""

converter_10_raw2 = Instrument("Module2-NI9215")  # #KKNREPLACEMENT
converter_10_raw2.attrs["comment"] = ""
converter_10_raw2.attrs["description"] = ""

# Pipeline object for VOLTAGE values of valve_position mesurement
channel_10_raw = Pipeline("measured/valve_position/raw")  # #KKNREPLACEMENT
channel_10_raw.add([converter_10_raw])
channel_10_raw.add([converter_10_raw2])
channel_10_raw.attrs["variable"] = "voltage"
channel_10_raw.attrs["units"] = "volts"
channel_10_raw.attrs["origin"] = "this"

# valve_position scaled
# set necessary Parameters for computing model
param_kh1 = Parameter("gain")
param_kh1.attrs["value"] = 13.355
param_kh1.attrs["units"] = "#open/volts"
param_kh1.attrs["variable"] = "ball valve position/voltage"
param_kh1.attrs["origin"] = "datasheet"

param_kh2 = Parameter("offset")
param_kh2.attrs["value"] = -23.904
param_kh2.attrs["units"] = "#open"
param_kh2.attrs["variable"] = "ball valve position"
param_kh2.attrs["origin"] = "datasheet"

# computing model of KH_position values
model = Model("linear")  # #KKNREPLACEMENT
model.add([param_kh1, param_kh2])

# Instrument object for measurement device
converter_10 = Instrument("ball_valve")  # #KKNREPLACEMENT
converter_10.add([model])
converter_10.attrs["device_type"] = " KVTF-L6-AJA-B12-DN25/20-D15-B02-PN40"
converter_10.attrs["manufacturer"] = "AUMA Riester GmbH & Co. KG"
converter_10.attrs["serial_number"] = "4017NS51189"
converter_10.attrs["timestamp_calibrated"] = ""
converter_10.attrs["input_range_min"] = 0
converter_10.attrs["input_range_max"] = 100
converter_10.attrs["input_units"] = "#offen"
converter_10.attrs["output_range_min"] = 0
converter_10.attrs["output_range_max"] = 10
converter_10.attrs["output_units"] = "volts"
converter_10.attrs["description"] = ""
converter_10.attrs["comment"] = "Output is 4-20mA with resistance of 470 Ohm"
converter_10.attrs["accuracy"] = ""
converter_10.attrs["accuracy_type"] = ""

# Pipeline object for valve_position values of voltage measurement
channel_10_scaled = Pipeline("measured/valve_position/scaled")
channel_10_scaled.add([converter_10])
channel_10_scaled.attrs["variable"] = "position"
channel_10_scaled.attrs["units"] = "#open"
channel_10_scaled.attrs["origin"] = "measured/valve_position/raw"


# add channels to file

msmtRun.add(
    [
        channel_01_raw,
        channel_01_scaled,
        channel_02_raw,
        channel_02_scaled,
        channel_03_raw,
        channel_03_scaled,
        channel_04_raw,
        channel_04_scaled,
        channel_05_raw,
        channel_05_scaled,
        channel_06_raw,
        channel_06_scaled,
        channel_07_raw,
        channel_07_scaled,
        channel_08_raw,
        channel_08_scaled,
        channel_09_raw,
        channel_09_scaled,
        channel_10_raw,
        channel_10_scaled,
    ]
)

msmtRun.store(format="json")
