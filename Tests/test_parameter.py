from pathlib import Path

import numpy as np
from pykkn.parameter import Parameter

test_result_path = Path("./test_result")
if not test_result_path.exists():
    test_result_path.mkdir()

para1 = Parameter("para1")
para1.attrs["value"] = 1
para1.attrs["units"] = "cm"
para1.attrs["variable"] = "-"
para1.attrs["origin"] = "this"
para1.set_storage_path(test_result_path / "parameter.h5")
para1.store()
para1.store(format="json")


def test_name():
    assert para1.name == "para1"


def test_value():
    assert para1.data == np.array(1).astype(np.double)


def test_other_attrs():
    assert para1.attrs["units"] == "cm"
    assert para1.attrs["variable"] == "-"
    assert para1.attrs["origin"] == "this"


def test_other_attributes():
    assert para1.storage_path == str(Path(test_result_path / "parameter.json"))
    assert para1.is_dataset is True
    assert para1.attrs["kkn_CLASS"] == "PARAMETER"
    assert para1.attrs["kkn_PARAMETER_VERSION"] == "1.0"


def test_file_exist():
    assert Path(test_result_path / "parameter.h5").exists()
