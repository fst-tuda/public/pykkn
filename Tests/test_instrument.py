from pathlib import Path

import pytest
from pykkn.instrument import Instrument
from pykkn.model import Model
from pykkn.parameter import Parameter

test_result_path = Path("./test_result")
if not test_result_path.exists():
    test_result_path.mkdir()

para1 = Parameter("para1")
para1.attrs["value"] = 1
para1.attrs["units"] = "cm"
para1.attrs["variable"] = "-"
para1.attrs["origin"] = "this"

para2 = Parameter("para2")
para2.attrs["value"] = 2
para2.attrs["units"] = "mm"
para2.attrs["variable"] = "-"
para2.attrs["origin"] = "this"

model1 = Model("model1")
model1.add([para1, para2])

model2 = Model("model2")
model2.add([para2, para1])

instr1 = Instrument("instr1")
instr1.add([model1, model2])

instr1.set_storage_path(test_result_path / "instrument.h5")
instr1.store()
instr1.store(format="json")


def test_name():
    assert instr1.name == "instr1"


def test_params():
    assert len(instr1.model) == 2
    assert instr1.model[0] == model1
    assert instr1.model[1] == model2


def test_wrong_para_input():
    with pytest.raises(AssertionError):
        instr1.add(model1)
    with pytest.raises(AssertionError):
        instr1.add([])
    with pytest.raises(TypeError):
        instr1.add([instr1])


def test_other_attributes():
    assert instr1.storage_path == str(Path(test_result_path
                                           / "instrument.json"))
    assert instr1.is_dataset is False
    assert instr1.attrs["kkn_CLASS"] == "INSTRUMENT"
    assert instr1.attrs["kkn_INSTRUMENT_VERSION"] == "1.0"


def test_file_exist():
    assert Path(test_result_path / "instrument.h5").exists()
