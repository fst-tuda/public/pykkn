from pathlib import Path

import numpy as np
from pykkn.dataset import Dataset

test_result_path = Path("./test_result")
if not test_result_path.exists():
    test_result_path.mkdir()

dataset = Dataset("dataset1")
dataset.data = np.double(1)
dataset.attrs["samplerate"] = 1000
dataset.attrs["timestamp"] = "2022-06-12 10:39:11"
dataset.set_storage_path(test_result_path / "test_dataset.h5")
dataset.store()
dataset.store(format="json")


def test_name():
    assert dataset.name == "dataset1"


def test_data():
    assert dataset.data == 1


def test_data_type():
    assert np.issubdtype(dataset.data.dtype, float)


def test_samplerate_value():
    assert dataset.attrs["samplerate"] == 1000


def test_samplerate_type():
    assert isinstance(dataset.attrs["samplerate"], float)


def test_timestamp():
    assert dataset.attrs["timestamp"] == "2022-06-12 10:39:11"


def test_other_attributes():
    assert dataset.storage_path == str(Path(test_result_path
                                            / "test_dataset.json"))
    assert dataset.is_dataset is True
    assert dataset.attrs["kkn_CLASS"] == "DATASET"
    assert dataset.attrs["kkn_DATASET_SUBCLASS"] == "TIMESERIES"
    assert dataset.attrs["kkn_TIMESERIES_VERSION"] == "1.0"


def test_file_exist():
    assert Path(test_result_path / "test_dataset.h5").exists()
