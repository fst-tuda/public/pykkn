from pathlib import Path

import numpy as np
import pytest
from pykkn.dataset import Dataset
from pykkn.instrument import Instrument
from pykkn.model import Model
from pykkn.parameter import Parameter
from pykkn.pipeline import Pipeline
from pykkn.run import Run

test_result_path = Path("./test_result")
if not test_result_path.exists():
    test_result_path.mkdir()

para1 = Parameter("para1")
para1.attrs["value"] = 1
para1.attrs["units"] = "cm"
para1.attrs["variable"] = "-"
para1.attrs["origin"] = "this"

para2 = Parameter("para2")
para2.attrs["value"] = 2
para2.attrs["units"] = "mm"
para2.attrs["variable"] = "-"
para2.attrs["origin"] = "this"

model1 = Model("model1")
model1.add([para1, para2])

model2 = Model("model2")
model2.add([para2, para1])

instr1 = Instrument("instr1")
instr1.add([model1, model2])

instr2 = Instrument("instr2")
instr2.add([model2, model1])

dataset = Dataset("dataset1")
dataset.data = np.double(1)
dataset.attrs["samplerate"] = 1000
dataset.attrs["timestamp"] = "2022-06-14 12:54:11"

pipe1 = Pipeline("measured/capa1/raw")
pipe1.attrs["variable"] = "voltage"
pipe1.attrs["units"] = "volts"
pipe1.attrs["origin"] = "this"
pipe1.add([instr1, instr2])
pipe1.add([dataset])

pipe2 = Pipeline("derived/capa2/scaled")
pipe2.attrs["variable"] = "length"
pipe2.attrs["units"] = "cm"
pipe2.attrs["origin"] = "this"
pipe2.add([instr2, instr1])
pipe2.add([dataset])

run = Run("run1")
run.add([para1, para2])
run.add([pipe1, pipe2])

run.set_storage_path(test_result_path / "run.h5")
run.store()
run.store(format="json")


def test_name():
    assert run.name == "run1"


def test_instruments():
    assert len(run.parameters) == 2
    assert run.parameters[0] == para1
    assert run.parameters[1] == para2


def test_pipeline():
    assert len(run.pipelines) == 2
    assert run.pipelines[0] == pipe1
    assert run.pipelines[1] == pipe2


def test_wrong_para_input():
    with pytest.raises(AssertionError):
        run.add(para1)
    with pytest.raises(AssertionError):
        run.add([])
    with pytest.raises(TypeError):
        run.add([run])


def test_other_attributes():
    assert run.storage_path == str(Path(test_result_path / "run.json"))
    assert run.is_dataset is False
    assert run.attrs["kkn_CLASS"] == "MSMTRUN"
    assert run.attrs["kkn_MSMTRUN_VERSION"] == "1.0"


def test_file_exist():
    assert Path(test_result_path / "run.h5").exists()
