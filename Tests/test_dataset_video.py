from pathlib import Path

import requests
from pykkn.dataset_video import Dataset_Video

url = "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4"

tmp_path = Path("./.tmp")
if not tmp_path.exists():
    tmp_path.mkdir()

r = requests.get(url)
with open(tmp_path / "test.mp4", "wb") as f:
    f.write(r.content)

test_result_path = Path("./test_result")
if not test_result_path.exists():
    test_result_path.mkdir()

dataset = Dataset_Video("video_dataset_1")
dataset.data = tmp_path / "test.mp4"
dataset.attrs["timestamp"] = "2022-06-13 11:22:11"
dataset.set_storage_path(test_result_path / "dataset_video.h5")

dataset.store()
dataset.store(format="json")


def test_name():
    assert dataset.name == "video_dataset_1"


def test_video_attrs():
    assert dataset.attrs["video_fps"] == 23
    assert dataset.attrs["video_height"] == 360
    assert dataset.attrs["video_length"] == "62s"
    assert dataset.attrs["video_num_frames"] == 1440
    assert dataset.attrs["video_width"] == 640
    assert dataset.attrs["file_name"] == "test.mp4"
    assert dataset.attrs["file_suffix"] == "mp4"


def test_timestamp():
    assert dataset.attrs["timestamp"] == "2022-06-13 11:22:11"


def test_other_attributes():
    assert dataset.storage_path == str(Path(test_result_path
                                            / "dataset_video.json"))
    assert dataset.is_dataset is True
    assert dataset.attrs["kkn_CLASS"] == "DATASET"
    assert dataset.attrs["kkn_DATASET_SUBCLASS"] == "VIDEO"
    assert dataset.attrs["kkn_TIMESERIES_VERSION"] == "1.0"


def test_file_exist():
    assert Path(test_result_path / "dataset_video.h5").exists()
    assert Path(test_result_path / "dataset_video.json").exists()
