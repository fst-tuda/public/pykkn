from pathlib import Path

import requests
from pykkn.dataset_image import Dataset_Image

url = 'https://www.learningcontainer.com/wp-content/uploads/2020/07/sample' \
      '-image-files-for-testing-300x300.png?ezimgfmt=rs:300x300/rscb4/ngcb' \
      '4/notWebP'

tmp_path = Path('./.tmp')
if not tmp_path.exists():
    tmp_path.mkdir()

r = requests.get(url)
with open(tmp_path / 'test.png', 'wb') as f:
    f.write(r.content)

test_result_path = Path('./test_result')
if not test_result_path.exists():
    test_result_path.mkdir()

dataset = Dataset_Image("image_dataset")
dataset.data = tmp_path / 'test.png'
dataset.set_storage_path(test_result_path / "dataset_image.h5")
dataset.store()
dataset.store(format="json")


def test_name():
    assert dataset.name == "image_dataset"


# here for image data content, we will not check its data type
# because in pillow lib, there are lots of classes for image
# such as PngImageFile, JrpgImageFIle etc.
# so it is too complicated to check
# def test_data_type():
#         assert isinstance(dataset.data, PIL.PngImagePlugin.PngImageFile)

# also here we will only check necessary attributes for a dataset instance
def test_image_attr():
    assert dataset.attrs["CLASS"] == "IMAGE"
    # assert dataset.attrs['IMAGE_MINMAXRANGE'] == np.array([0, 255])
    assert dataset.attrs["IMAGE_SUBCLASS"] == "IMAGE_TRUECOLOR"
    # assert dataset.attrs['IMAGE_VERSION'] == np.string_('1.2')
    # assert dataset.attrs['INTERLACE_MODE'] == np.string_('INTERLACE_PIXEL')

    assert dataset.attrs["file_name"] == "test.png"
    assert dataset.attrs["file_format"] == "PNG"
    assert dataset.attrs["image_mode"] == "RGBA"

    assert dataset.attrs["shape"] == (300, 300, 4)
    assert dataset.attrs["image_width"] == 300
    assert dataset.attrs["image_height"] == 300
    assert dataset.attrs["num_channels"] == 4
    assert dataset.attrs["data_type"] == "uint8"


def test_other_attributes():
    # With opencv as an example (Pillow would be the exact same)
    assert dataset.storage_path == str(Path(
        test_result_path / "dataset_image.json"))
    assert dataset.is_dataset is True
    assert dataset.attrs["kkn_CLASS"] == "DATASET"
    assert dataset.attrs["kkn_DATASET_SUBCLASS"] == "IMAGE"
    assert dataset.attrs["kkn_DATASET_VERSION"] == "1.0"


def test_file_exist():
    assert Path(test_result_path / "dataset_image.h5").exists()
    assert Path(test_result_path / "dataset_image.json").exists()


# TODO check using reading HDF5 file and JSON file
