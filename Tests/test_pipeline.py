from pathlib import Path

import numpy as np
import pytest
from pykkn.dataset import Dataset
from pykkn.instrument import Instrument
from pykkn.model import Model
from pykkn.parameter import Parameter
from pykkn.pipeline import Pipeline

test_result_path = Path("./test_result")
if not test_result_path.exists():
    test_result_path.mkdir()

para1 = Parameter("para1")
para1.attrs["value"] = 1
para1.attrs["units"] = "cm"
para1.attrs["variable"] = "-"
para1.attrs["origin"] = "this"

para2 = Parameter("para2")
para2.attrs["value"] = 2
para2.attrs["units"] = "mm"
para2.attrs["variable"] = "-"
para2.attrs["origin"] = "this"

model1 = Model("model1")
model1.add([para1, para2])

model2 = Model("model2")
model2.add([para2, para1])

instr1 = Instrument("instr1")
instr1.add([model1, model2])

instr2 = Instrument("instr2")
instr2.add([model2, model1])

dataset = Dataset("dataset1")
dataset.data = np.double(1)
dataset.attrs["samplerate"] = 1000
dataset.attrs["timestamp"] = "2022-06-14 12:54:11"

pipe1 = Pipeline("measured/capa1/raw")
pipe1.attrs["variable"] = "voltage"
pipe1.attrs["units"] = "volts"
pipe1.attrs["origin"] = "this"
pipe1.add([instr1, instr2])
pipe1.add([dataset])


pipe1.set_storage_path(test_result_path / "pipeline.h5")
pipe1.store()
pipe1.store(format="json")


def test_name():
    assert pipe1.name == "measured/capa1/raw"


def test_instruments():
    assert len(pipe1.instruments) == 2
    assert pipe1.instruments[0] == instr1
    assert pipe1.instruments[1] == instr2


def test_dataset():
    assert len(pipe1.data) == 1
    assert pipe1.data[0] == dataset


def test_wrong_para_input():
    with pytest.raises(AssertionError):
        pipe1.add(instr1)
    with pytest.raises(AssertionError):
        pipe1.add([])
    with pytest.raises(TypeError):
        pipe1.add([pipe1])


def test_other_attributes():
    assert pipe1.storage_path == str(Path(test_result_path / "pipeline.json"))
    assert pipe1.is_dataset is False
    assert pipe1.attrs["kkn_CLASS"] == "PIPELINE"
    assert pipe1.attrs["kkn_PIPELINE_VERSION"] == "1.0"


def test_file_exist():
    assert Path(test_result_path / "pipeline.h5").exists()
