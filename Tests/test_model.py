from pathlib import Path

import pytest
from pykkn.model import Model
from pykkn.parameter import Parameter

test_result_path = Path("./test_result")
if not test_result_path.exists():
    test_result_path.mkdir()

para1 = Parameter("para1")
para1.attrs["value"] = 1
para1.attrs["units"] = "cm"
para1.attrs["variable"] = "-"
para1.attrs["origin"] = "this"

para2 = Parameter("para2")
para2.attrs["value"] = 2
para2.attrs["units"] = "mm"
para2.attrs["variable"] = "-"
para2.attrs["origin"] = "this"

model1 = Model("model1")
model1.add([para1, para2])
model1.set_storage_path(test_result_path / "model.h5")
model1.store()
model1.store(format="json")


def test_name():
    assert model1.name == "model1"


def test_params():
    assert len(model1.parameters) == 2
    assert model1.parameters[0] == para1
    assert model1.parameters[1] == para2


def test_values():
    assert model1.parameters[0].data == 1
    assert model1.parameters[1].data == 2


def test_wrong_para_input():
    with pytest.raises(AssertionError):
        model1.add(para1)
    with pytest.raises(AssertionError):
        model1.add([])
    with pytest.raises(TypeError):
        model1.add([model1])


def test_other_attributes():
    assert model1.storage_path == str(Path(test_result_path / "model.json"))
    assert model1.is_dataset is False
    assert model1.attrs["kkn_CLASS"] == "MODEL"
    assert model1.attrs["kkn_MODEL_SUBCLASS"] == "POLY"
    assert model1.attrs["kkn_POLY_VERSION"] == "1.0"


def test_file_exist():
    assert Path(test_result_path / "model.h5").exists()
