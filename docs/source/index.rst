.. PYKKN documentation master file, created by
   sphinx-quickstart on Fri Jun 24 07:40:01 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PYKKN's documentation!
=================================

.. toctree::
   :maxdepth: 1
   :hidden:

   Getting Started <start/index>
   User Guide <user/index>
   API reference <reference/index>



**Useful links**:
`Installation <https://en.wikipedia.org/wiki/Installation_(computer_programs)>`_ |
`Source Repository <https://git.rwth-aachen.de/fst-tuda/projects/rdm/adp-pykraken/pykkn>`_ 

PYKKN is a library that supports researchers to store their data in a HDF5 file using Python.
The library can handle different types of inputs like ....


.. panels::
    :card: + intro-card text-center
    :column: col-lg-6 col-md-6 col-sm-6 col-xs-12 d-flex

    :img-top: ../source/_static/index-images/getting-started.jpg

    Getting Started
    ^^^^^^^^^^^^^^^

    First time using the PYKKN library? Here we will show you how to install the library and set up your environment.

    +++

    .. link-button:: start/index
        :type: ref
        :text: To the first steps
        :classes: btn-block btn-secondary stretched-link
    
    ---
    :img-top: ../source/_static/index-images/user-guide.png

    User Guide
    ^^^^^^^^^^

    Here, you can see an example of how to use PYKKN to save data of a scientific experiment in a HDF5 file.

    +++

    .. link-button:: user/index
        :type: ref
        :text: To the user guide
        :classes: btn-block btn-secondary stretched-link

    ---
    :img-top: ../source/_static/index-images/api.png

    API Reference
    ^^^^^^^^^^^^^^^

    The reference guide contains a detailed description of the functions,
    modules, and objects included in PYKKN. The reference describes how the
    methods work and which parameters can be used.

    +++

    .. link-button:: reference/index
        :type: ref
        :text: To the API reference
        :classes: btn-block btn-secondary stretched-link





Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
