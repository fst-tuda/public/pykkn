Getting Started
*****************


1. We recommend using Anaconda or Miniconda to create and manage your virtual environment. The purpose of this is to establish a package environment for each project so that dependencies between projects do not conflict with each other. Detailed installation tutorials can be found at [https://www.anaconda.com/products/distribution] and [https://docs.conda.io/en/latest/miniconda.html].

2. If you are using Windows, you can verify that your installation was successful by using conda --version in Anaconda Powershell Prompt or Anaconda Prompt. If you are using a Unix/Linux system, you can run the same command in the terminal to verify whether your installation was successful. At the same time, if not specified below, all commands are executed in the Anaconda Powershell Prompt / Anaconda Prompt / Terminal that has activated the corresponding environment.
At the same time, it should be noted that the writing and tutorials of this tool are based on the Windows system and python=3.10. If you encounter difficulties, you are welcome to give feedback on the relevant GitLab pages.

3. Create a virtual environment and activate it::

    conda create -n pykkn_env python=3.10
    conda activate pykkn_env



4. Install PYKKN by using pip install::

    pip install pykkn

