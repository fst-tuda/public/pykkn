:py:mod:`parse_json`
====================

.. py:module:: parse_json


Module Contents
---------------


Functions
~~~~~~~~~

.. autoapisummary::

   parse_json.create_instance
   parse_json.recursive_create_instance
   parse_json.pykkn_parse



.. py:function:: create_instance(dic)

   create the corresponding object

   :param dic: json structure or sub-structure
   :type dic: dict

   :returns: one of the component types
             the type of return depends on the structure of json structure
   :rtype: object

   :raises TypeError: raised when given a wrong dataset class
   :raises TypeError: raised when given a wrong component class


.. py:function:: recursive_create_instance(file)

   Recursively read json structure
   create the corresponding object and assign the original property value

   :param file: json structure or sub-structure
   :type file: dict

   :returns: one of the component types
             the type of return depends on the structure of json file
   :rtype: object


.. py:function:: pykkn_parse(path)

   read a json file and convert it to the pykkn data management structure

   :param path: path of the object json file
   :type path: str

   :returns: one of the component types
             the type of return depends on the structure of json file
   :rtype: object


