:py:mod:`components.parameter`
==============================

.. py:module:: components.parameter


Module Contents
---------------

Classes
~~~~~~~

.. autoapisummary::

   components.parameter.Parameter




.. py:class:: Parameter(name)

   Bases: :py:obj:`Storage`

   An object of this class represents the Parameter.

   :param name: the name of the parameter
   :type name: str

   .. rubric:: Examples

   para1 = Parameter('para1')

   para1.attrs['value'] = 1
   para1.attrs['units'] = 'cm'

   para1.attrs['variable'] = '-'

   para1.attrs['origin'] = 'this'


