:py:mod:`components.model`
==========================

.. py:module:: components.model


Module Contents
---------------

Classes
~~~~~~~

.. autoapisummary::

   components.model.Model




.. py:class:: Model(name)

   Bases: :py:obj:`Storage`

   This object is used to represent the model for data processing in the experiment

   :param name: the name of the model
   :type name: str

   .. rubric:: Examples

   model = Model('model1')

   model.add([parameter1, parameter2])

   .. py:method:: add(self, list_obj)

      add (multi) parameter(s) into model

      :param list_obj: a list of Parameter object(s)
      :type list_obj: List[Parameter]

      :raises AssertionError:: raise when add method fails



