:py:mod:`test_model`
====================

.. py:module:: test_model


Module Contents
---------------


Functions
~~~~~~~~~

.. autoapisummary::

   test_model.test_name
   test_model.test_params
   test_model.test_values
   test_model.test_wrong_para_input
   test_model.test_other_attributes
   test_model.test_file_exist



Attributes
~~~~~~~~~~

.. autoapisummary::

   test_model.para1
   test_model.para2
   test_model.model1


.. py:data:: para1
   

   

.. py:data:: para2
   

   

.. py:data:: model1
   

   

.. py:function:: test_name()


.. py:function:: test_params()


.. py:function:: test_values()


.. py:function:: test_wrong_para_input()


.. py:function:: test_other_attributes()


.. py:function:: test_file_exist()


