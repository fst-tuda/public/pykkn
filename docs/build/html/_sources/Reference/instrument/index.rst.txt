:py:mod:`instrument`
====================

.. py:module:: instrument


Module Contents
---------------

Classes
~~~~~~~

.. autoapisummary::

   instrument.Instrument




.. py:class:: Instrument(name)

   Bases: :py:obj:`pykkn.storage.Storage`

   This class represents Datasets that are mapped from other datasets
   using a given model

   This can be used to convert the input of a sensor to its physical data
   using the given model
   for example polynomials or lookup tables.

   :param name: the name of the instrument
   :type name: str

   .. rubric:: Examples

   instrument = Instrument('instrument1')

   instrument.add([model1, model2])

   .. py:method:: add(self, list_obj)

      add (multi) model(s) into instrument

      :param list_obj: a list of Model object(s)
      :type list_obj: List[Model]

      :raises AssertionError:: raise when adding method fails



