:py:mod:`test_run`
==================

.. py:module:: test_run


Module Contents
---------------


Functions
~~~~~~~~~

.. autoapisummary::

   test_run.test_name
   test_run.test_instruments
   test_run.test_pipeline
   test_run.test_wrong_para_input
   test_run.test_other_attributes
   test_run.test_file_exist



Attributes
~~~~~~~~~~

.. autoapisummary::

   test_run.para1
   test_run.para2
   test_run.model1
   test_run.model2
   test_run.instr1
   test_run.instr2
   test_run.dataset
   test_run.data
   test_run.pipe1
   test_run.pipe2
   test_run.run


.. py:data:: para1
   

   

.. py:data:: para2
   

   

.. py:data:: model1
   

   

.. py:data:: model2
   

   

.. py:data:: instr1
   

   

.. py:data:: instr2
   

   

.. py:data:: dataset
   

   

.. py:data:: data
   

   

.. py:data:: pipe1
   

   

.. py:data:: pipe2
   

   

.. py:data:: run
   

   

.. py:function:: test_name()


.. py:function:: test_instruments()


.. py:function:: test_pipeline()


.. py:function:: test_wrong_para_input()


.. py:function:: test_other_attributes()


.. py:function:: test_file_exist()


