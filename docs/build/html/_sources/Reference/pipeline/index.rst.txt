:py:mod:`pipeline`
==================

.. py:module:: pipeline


Module Contents
---------------

Classes
~~~~~~~

.. autoapisummary::

   pipeline.Pipeline




.. py:class:: Pipeline(name)

   Bases: :py:obj:`pykkn.storage.Storage`

   This object represents a pipeline

   This name of pipeline should contain the following information:

   <measured/derived>/<capa>/<raw/scaled>

   :param name: the name of the pipeline
   :type name: str

   .. rubric:: Example

   pipeline1 = Pipeline('measured/capa1/raw')

   pipeline1.attrs['variable'] = 'voltage'

   pipeline1.attrs['units'] = 'volts'

   pipeline1.attrs['origin'] = 'this'


   pipeline.add([dataset1, dataset2])

   pipeline.add([instrument1, insturment2])

   .. py:method:: add(self, list_obj)

      add (multi) dataset(s) and instrument(s) into model

      :param list_obj: a list of Instrument or Dataset object(s)
      :type list_obj: List[Instrument  |  Dataset]

      :raises TypeError: raised when the element is not the type of Instrument
          or Dataset
      :raises AssertionError: raised when list_obj is not a list or it is empty



