:py:mod:`storage`
=================

.. py:module:: storage


Module Contents
---------------

Classes
~~~~~~~

.. autoapisummary::

   storage.Storage




.. py:class:: Storage(name)

   This class is abstracted for all other classes
   providing initialization function with a name
   and store function to generate an HDF5 file

   :param name: the name of the instant
   :type name: str

   .. py:method:: set_storage_path(self, path)

      set the storage path where you want to store the HDF5 file

      .. attention:: please always use "/" in path string instead of ""

      :param path: the storage path you want to store the result file,
                   have to end with .h5
      :type path: str


   .. py:method:: store_HDF5(self, root=None)

      sub-method of store() for HDF5 output

      :param root: the root of file object
      :type root: h5py.File, optional


   .. py:method:: store_json(self, root=None)

      sub-method of store() for JSON output

      :param root: the root of dict object
      :type root: dict, optional


   .. py:method:: store(self, format=None, root=None)

      store the pykkn structure into HDF5 or JSON

      :param format: the target format, by default is HDF5, can be specified as json
      :type format: str, optional
      :param root: the root of h5py.File or dict object
      :type root: h5py.File or dict, optional


   .. py:method:: __str__(self)

      rewrite the built-in method to modify the behaviors of print()
      to make the print result more readable

      before:

      >>>print(run1)

      <run.Run object at 0x0000022AA45715A0>

      after:

      >>>print(run1)

      'run1'

      here, the string 'run1' is the name of this instance


   .. py:method:: __repr__(self)

      rewrite the built-in method to modify the behaviors of print()
      to make the print result more readable

      before:

      >>>print(run1.pipelines)

      [<pipeline.Pipeline object at 0x0000022AA45715A0>]

      after:

      >>>print(run1.pipelines)

      ['pipe1']

      here, the strings 'pipe1' and 'pipe2' are the names of this instance


   .. py:method:: show(self)

      use the method to show the detailed information about this instance
      for example all attributes and names.
      It should return a string like this:

      .. rubric:: Examples

      >> msmtrun.show()

      #### pipelines ####

      ['aa', 'bb', 'cc']

      #### parameters ####

      ['dd', 'ee', 'ff']

      #### attributes ####

      'author' : 'who'

      'author' : 'derGeraet'

      'pmanager' : 'tcorneli'

      'targettmp' : 70

      'targetrps' : 2

      'oil' : 'PA06'


   .. py:method:: add_attrs_dict(self, dict)

      Add a flat Dictionary of key values as a set of attributes.

      Parameters:
      ------------
      dict : str
          The Dictionary consists of Key Value pairs
          with the keys being the names of the attribute
          and the value being the value assigned to the attribute


   .. py:method:: encode(self, object)

      encode anything as a string

      Parameters:
      -----------
      object: Any
          an object which can be an instance of any class

      Returns:
      --------
      object_string: str
          an encoded string, maybe very long if the original object is large


   .. py:method:: decode(self, object_string)

      decode a string as its original form

      Parameters:
      -----------
      object_string: str
          an encoded string

      Returns:
      --------
      object: object
          this is a instance of its original class



